
package com.briplomo.utils;

import com.briplomo.models.SatelliteDataEntry;
import com.briplomo.models.Violation;

public enum Severity {
    RED_HIGH("RED HIGH"){
        public Violation getViolation(SatelliteDataEntry entry) {
            if(entry.getValue() > entry.getRedHighLimit()) {
                return new Violation(Severity.RED_HIGH,entry.getComponent(),entry.getTimestamp(), entry.getSatelliteID());
            }
            return null;
        }
    }, RED_LOW("RED LOW"){
        public Violation getViolation(SatelliteDataEntry entry) {
            if(entry.getValue() < entry.getRedLowLimit()) {
                return new Violation(Severity.RED_LOW,entry.getComponent(),entry.getTimestamp(), entry.getSatelliteID());
            }
            return null;
        }
    },YELLOW_LOW("YELLOW_LOW"){
        public Violation getViolation(SatelliteDataEntry entry) {
            if(entry.getValue() < entry.getYellowLowLimit()) {
                return new Violation(Severity.YELLOW_LOW,entry.getComponent(),entry.getTimestamp(), entry.getSatelliteID());
            }
            return null;
        }
    }, YELLOW_HIGH("YELLOW_HIGH"){
        public Violation getViolation(SatelliteDataEntry entry) {
            if(entry.getValue() > entry.getYellowHighLimit()) {
                return new Violation(Severity.YELLOW_HIGH,entry.getComponent(),entry.getTimestamp(), entry.getSatelliteID());
            }
            return null;
        }
    }
    ;

    private final String text;
    Severity(String text) {
        this.text = text;
    }

    public String toString() {
        return this.text;
    }

    public abstract Violation getViolation(SatelliteDataEntry entry);
}
