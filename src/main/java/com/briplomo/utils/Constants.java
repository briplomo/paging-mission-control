package com.briplomo.utils;

public final class Constants {
    public static final int BATTERY_TIME_INTERVAL_SECS = 300;
    public static final int THERMOSTAT_TIME_INTERVAL_SECS = 300;
    public static final int BATTERY_THRESHOLD = 3;
    public static final int THERMOSTAT_THRESHOLD = 3;
    public static final Severity THERMOSTAT_SEVERITY = Severity.RED_HIGH;
    public static final Severity BATTERY_SEVERITY = Severity.RED_LOW;
    public static final String SATELLITE_TELEMETRY_PATH = "./src/test/resources/SatelliteTelemetry.txt";

}
