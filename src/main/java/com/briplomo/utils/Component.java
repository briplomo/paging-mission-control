package com.briplomo.utils;

import com.briplomo.models.Alert;
import com.briplomo.models.SatelliteDataEntry;
import com.briplomo.models.Violation;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Getter
@Setter

public abstract class Component {

    protected List<Violation> violations;
    protected Severity SEVERITY;
    protected int TIME_INTERVAL_SECS;
    protected int THRESHOLD;


    /**
     * Iterates through list of found violations. Uses predefined constants to determine
     * max number of occurrences allowed within predefined time interval.
     * As is this function will continue creating alerts as long as the number of occurrences within
     * TIME_INTERVAL_SECS is greater than or equal to protected int THRESHOLD.
     * This may lead to seemingly duplicate alerts as the first {@link Violation} within the timeframe is
     * used to populate the timestamp of the {@link Alert}
     * @see Constants
     * @return a list of {@link Alert}
     * */
    public List<Alert> getAlerts() {
        List<Alert> alerts = new ArrayList<>();
        int left = 0;
        int right = 0;

        while(right < violations.size()) {
            while((int)(violations.get(right).getTimestamp().toEpochSecond()-violations.get(left).getTimestamp().toEpochSecond()) > TIME_INTERVAL_SECS){
                left++;
            }
            if (right-left+1 >= THRESHOLD) {
                Violation v = violations.get(left);
                alerts.add(Alert.builder()
                                .component(v.getComponent().getString())
                                .satelliteID(v.getSatelliteID())
                                .timestamp(v.getTimestamp().toString())
                                .severity(v.getSeverity().toString())
                        .build()
                );
            }
            right++;
        }
        return alerts;
    }

    /**
     * Checks if an instance of {@link SatelliteDataEntry} is in violation of its and the components predefined parameters.
     * */
    private Optional<Violation> isViolation(SatelliteDataEntry entry){
        return Optional.ofNullable(this.getSEVERITY().getViolation(entry));
    }

    /**
     * If a given entry is in violation of its defined parameters a {@link Violation} object is
     * created and added to an instance variable list of Violation.
     * */
    public void addEntry(SatelliteDataEntry entry) {
        Optional<Violation> violation = isViolation(entry);
        violation.ifPresent(value -> violations.add(value));
    }



}
