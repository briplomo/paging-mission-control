package com.briplomo.utils;

public enum ComponentType {
    THERMOSTAT("TSTAT"), BATTERY("BATT");
    private final String text;
    ComponentType(String text) {
        this.text = text;
    }

    public String getString() {
        return this.text;
    }

    /**
     * Creates a component from a String by comparing enum value text field.
     * @param text String to create ComponentType from
     * @return ComponentType
     * */
    public static ComponentType fromString(String text) {
        for (ComponentType c : ComponentType.values()) {
            if (c.text.equalsIgnoreCase(text)) {
                return c;
            }
        }
        throw new IllegalArgumentException("No Component enum with text" + text + "found.");
    }
}
