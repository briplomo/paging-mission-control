package com.briplomo.models;

import com.briplomo.utils.Component;

import java.util.ArrayList;

import static com.briplomo.utils.Constants.*;

/**
 * Instance of a Component. Holds Battery specific constants used in {@link Component} methods.
 * @see Component
 * */
public class Battery extends Component {

    public Battery(){
        this.TIME_INTERVAL_SECS = BATTERY_TIME_INTERVAL_SECS;
        this.SEVERITY = BATTERY_SEVERITY;
        this.THRESHOLD = BATTERY_THRESHOLD;
        this.violations = new ArrayList<>();
    }

}
