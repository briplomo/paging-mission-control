package com.briplomo.models;

import com.briplomo.utils.ComponentType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

/**
 * Holds data for a line of satellite telemetry data
 * Overrides compareTo method to allow sorting by timestamp
 * */
@Getter
@Setter
@Builder
public class SatelliteDataEntry implements Comparable<SatelliteDataEntry>{
    private int satelliteID;
    private ZonedDateTime timestamp;
    private ComponentType component;
    private float value;
    private float yellowLowLimit;
    private float yellowHighLimit;
    private float redLowLimit;
    private float redHighLimit;

    @Override
    public int compareTo(SatelliteDataEntry o) {
        return this.timestamp.compareTo(o.getTimestamp());
    }

}
