package com.briplomo.models;

import com.briplomo.utils.Component;
import com.briplomo.utils.ComponentType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Data class holds telemetry data in the form of components.
 * Satellite instance adds telemetry data entries to their corresponding {@link Component}.
 * Calls each of its components to identify and return data signifying an alert-worthy event.
 * @see Component
 * */
@AllArgsConstructor
@Setter
@Getter
public class Satellite {
    private int satelliteID;
    private Battery battery;
    private Thermostat thermostat;

    /**
     * @return a list of {@link Alert} objects created from having each component identify
     * and return a list of Alert from its corresponding telemetry data
     * */
    public Optional<List<Alert>> getAlerts() {
        List<Alert> batteryAlerts = battery.getAlerts();
        List<Alert> thermostatAlerts = thermostat.getAlerts();
        if (thermostatAlerts.isEmpty() && batteryAlerts.isEmpty()) {
            return Optional.empty();
        } else if (thermostatAlerts.isEmpty()) {
            return Optional.of(batteryAlerts);
        } else if (batteryAlerts.isEmpty()) {
            return Optional.of(thermostatAlerts);
        } else {
            List<Alert> allAlerts = new ArrayList<>(thermostatAlerts);
            allAlerts.addAll(batteryAlerts);
            return Optional.of(allAlerts);
        }
    }

    /**
     * Takes a {@link SatelliteDataEntry} instance and hands it off to the corresponding {@link Component}
     * @param entry a {@link SatelliteDataEntry} instance
     * */
    public void addEntry(SatelliteDataEntry entry) {
        ComponentType type = entry.getComponent();
        if (type.equals(ComponentType.BATTERY)) {
            battery.addEntry(entry);
        }
        else if (type.equals(ComponentType.THERMOSTAT)) {
            thermostat.addEntry(entry);
        }
    }
}
