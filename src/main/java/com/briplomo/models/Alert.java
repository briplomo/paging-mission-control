package com.briplomo.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;


@Getter
@Builder
@AllArgsConstructor
public class Alert {
    private int satelliteID;
    private String severity;
    private String component;
    private String timestamp;
}
