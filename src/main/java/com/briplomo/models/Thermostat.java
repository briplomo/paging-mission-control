package com.briplomo.models;

import com.briplomo.utils.Component;
import lombok.Getter;

import java.util.ArrayList;

import static com.briplomo.utils.Constants.*;

/**
 * Instance of a Component. Holds thermostat specific constants used in parent class methods.
 * @see Component
 * */
@Getter
public class Thermostat extends Component {
    public Thermostat(){
        this.TIME_INTERVAL_SECS = THERMOSTAT_TIME_INTERVAL_SECS;
        this.SEVERITY = THERMOSTAT_SEVERITY;
        this.THRESHOLD = THERMOSTAT_THRESHOLD;
        this.violations = new ArrayList<>();
    }
}
