package com.briplomo.models;

import com.briplomo.utils.ComponentType;
import com.briplomo.utils.Severity;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.ZonedDateTime;

/**
 * Holds information relevant to identifying a violation of a components normal parameters.
 * */
@AllArgsConstructor
@Getter
public class Violation {
    private Severity severity;
    private ComponentType component;
    private ZonedDateTime timestamp;
    private int satelliteID;
}
