package com.briplomo.models;

import com.briplomo.utils.Component;
import com.briplomo.utils.ComponentType;
import com.briplomo.utils.Constants;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import java.io.*;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.briplomo.utils.Constants.SATELLITE_TELEMETRY_PATH;

/**
 * Main handler class responsible for creating alerts from telemetry.
 * Parses all data from text file whose path is specified in {@link Constants}.
 * Identifies alert-worthy data and then returns alerts in the form of JSON strings.
 *
 **/
@Setter
@Getter
public class SatelliteTelemetryHandler {

    private static final HashMap<Integer,Satellite> satellites = new HashMap<>();
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private String filePath = SATELLITE_TELEMETRY_PATH;

    /**
     * Parses telemetry data into instances of {@link SatelliteDataEntry} from text file.
     * Places data entries into corresponding {@link Satellite} objects.
     * @throws IOException if unable to read or find file
     */
    private void parseTelemetryData() throws IOException {
        // Read file contents into buffered reader
        FileInputStream stream = new FileInputStream(filePath);
        DataInputStream inputStream = new DataInputStream(stream);
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

        List<SatelliteDataEntry> telemetryData = new ArrayList<>();

        String line;

        // Get SatelliteDataEntry object for each line in telemetry data
        while ((line = br.readLine()) != null) {
            String[] data = line.split("[|]");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyMMdd HH:mm:ss.SSS").withZone(ZoneOffset.UTC);
            SatelliteDataEntry dataEntry = SatelliteDataEntry.builder()
                    .timestamp(ZonedDateTime.parse(data[0], formatter))
                    .satelliteID(Integer.parseInt(data[1]))
                    .redHighLimit(Float.parseFloat(data[2]))
                    .yellowLowLimit(Float.parseFloat(data[3]))
                    .yellowHighLimit(Float.parseFloat(data[4]))
                    .redLowLimit(Float.parseFloat(data[5]))
                    .value(Float.parseFloat(data[6]))
                    .component(ComponentType.fromString(data[7]))
                    .build();
            telemetryData.add(dataEntry);
        }

        // Sort telemetry data by timestamp
        Collections.sort(telemetryData);

        // Add data to corresponding Satellite
        for (SatelliteDataEntry entry: telemetryData) {
            // Create and add new satellite if it does not exist
            satellites.computeIfAbsent(entry.getSatelliteID(), k -> new Satellite(entry.getSatelliteID(), new Battery(), new Thermostat()));
            satellites.get(entry.getSatelliteID()).addEntry(entry);
        }
    }


    /**
     * Calls getAlerts method on each Component object of each Satellite object and returns them in a List of Alerts
     * @see Satellite
     * @see Component
     * @return list of Alert objects
     * */
    private List<Alert> getAlerts(){
        List<Alert> alerts = new ArrayList<>();
        for (Satellite satellite: satellites.values()){
            Optional<List<Alert>> satelliteAlerts = satellite.getAlerts();
            satelliteAlerts.ifPresent(alerts::addAll);
        }
        return alerts;
    }

    /**
     * Takes all Alert objects and converts them to JSON strings using Jackson
     * @param alerts List of Alert
     * @return List of JSON strings
     * */
    private List<String> alertsToJson(List<Alert> alerts) {
        List<String> jsonAlerts = new ArrayList<>();
        for (Alert alert:alerts) {
            try {
                String json = objectMapper.writeValueAsString(alert);
                jsonAlerts.add(json);
            } catch(Exception e) {
                System.out.println("Error converting Alert object to JSON: " + Arrays.toString(e.getStackTrace()));
            }
        }
        return jsonAlerts;
    }
    public List<String> run() {
        try {
            parseTelemetryData();
        } catch (IOException e) {
            System.out.println("An error occurred while parsing telemetry data. "+ Arrays.toString(e.getStackTrace()));
        } catch (Exception e) {
            System.out.println("An unknown exception occurred. \n"+ Arrays.toString(e.getStackTrace()));
        }
        List<Alert> alerts = getAlerts();
        return alertsToJson(alerts);
    }
}
