package com.briplomo;

import com.briplomo.models.SatelliteTelemetryHandler;

public class SatelliteStatusService {
    public static void main(String[] args) {
        final SatelliteTelemetryHandler handler = new SatelliteTelemetryHandler();
        System.out.print(handler.run());
    }
}