import com.briplomo.models.Alert;
import com.briplomo.models.Battery;
import com.briplomo.models.SatelliteDataEntry;
import com.briplomo.models.Violation;
import com.briplomo.utils.Component;
import com.briplomo.utils.ComponentType;
import com.briplomo.utils.Severity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ComponentTest {
    private List<Violation> violationsTest;
    private Component componentUnderTest;
    private ZonedDateTime expectedTimestamp;
    private SatelliteDataEntry testEntry;
    private List<Violation> testEmptyViolationList;

    private List<Alert> testEmptyAlerts;
    private Violation testViolation;

    @BeforeEach
    void setup() {
        violationsTest = new ArrayList<>();
        testEmptyAlerts = new ArrayList<>();
        testEmptyViolationList = new ArrayList<>();
        expectedTimestamp = ZonedDateTime.now();
        testViolation = new Violation(Severity.RED_LOW, ComponentType.BATTERY, expectedTimestamp,123456);
        violationsTest.add(testViolation);
        violationsTest.add(new Violation(Severity.RED_LOW, ComponentType.BATTERY, expectedTimestamp.plusMinutes(1),123456));
        violationsTest.add(new Violation(Severity.RED_LOW, ComponentType.BATTERY, expectedTimestamp.plusMinutes(2),123456));
        componentUnderTest = new Battery();
        testEntry = SatelliteDataEntry.builder()
                .satelliteID(123456)
                .component(ComponentType.BATTERY)
                .value(5)
                .timestamp(expectedTimestamp)
                .redLowLimit(7)
                .yellowLowLimit(8)
                .redHighLimit(25)
                .yellowHighLimit(24)
                .build();

    }
    @Test
    void getAlertsTest() {
        //Test with populated violations
        componentUnderTest.setViolations(violationsTest);
        List<Alert> expectedAlerts = new ArrayList<>();
        expectedAlerts.add(new Alert(123456, Severity.RED_LOW.toString(),ComponentType.BATTERY.getString(), expectedTimestamp.toString()));
        assertThat(componentUnderTest.getAlerts()).usingRecursiveComparison().isEqualTo(expectedAlerts);

        //Test with empty list of alerts
        List<Violation> emptyTestViolations = new ArrayList<>();
        componentUnderTest.setViolations(emptyTestViolations);
        assertThat(componentUnderTest.getAlerts()).usingRecursiveComparison().isEqualTo(testEmptyAlerts);
    }

    @Test
    void addEntryTest() {
        componentUnderTest.addEntry(testEntry);
        assertThat(componentUnderTest.getViolations().get(0)).usingRecursiveComparison().isEqualTo(testViolation);

        componentUnderTest.setViolations(testEmptyViolationList);
        testEntry.setValue(15);
        componentUnderTest.addEntry(testEntry);
        assertTrue(componentUnderTest.getViolations().isEmpty());
    }
}
