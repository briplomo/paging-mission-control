import com.briplomo.models.SatelliteTelemetryHandler;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static utils.Constants.TEST_TELEMETRY_DATA_PATH;


class SatelliteTelemetryHandlerTest {
    private AutoCloseable autoCloseable;
    private SatelliteTelemetryHandler underTest;

    @BeforeEach
    void setup() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        underTest = new SatelliteTelemetryHandler();
        underTest.setFilePath(TEST_TELEMETRY_DATA_PATH);

    }
    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void runFunctionTest() {
        List<String> expectedAlerts = new ArrayList<>();
            expectedAlerts.add("{\"satelliteID\":1000,\"severity\":\"RED HIGH\",\"component\":\"TSTAT\",\"timestamp\":\"2018-01-01T23:01:38.001Z\"}");
            expectedAlerts.add("{\"satelliteID\":1000,\"severity\":\"RED LOW\",\"component\":\"BATT\",\"timestamp\":\"2018-01-01T23:01:09.521Z\"}");
            expectedAlerts.add("{\"satelliteID\":1001,\"severity\":\"RED LOW\",\"component\":\"BATT\",\"timestamp\":\"2018-01-01T23:05:06.421Z\"}");
        List<String> jsonAlerts = underTest.run();
        assertEquals(expectedAlerts, jsonAlerts);

    }
}
